<?php

function debug($aVariable){
   
    if(is_array($aVariable)){
        echo "<pre>";
        print_r( $aVariable );
        echo "</pre>";
    }else{
        var_dump($aVariable);
    }   
}

debug($_POST);


?>

<!DOCTYPE html>

<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>  
        <body>    
                
    <section>
            <form action="deathcertificate.php" method="post">
                
                <h1>Appendix A Sample US Death Certificate Form</h1>
                <p>The sample death reporting form............</p>
                <h2>Death Reporting Form Vital Records</h2>
                
                <!--Start Decedent's Name Field-->
                <fieldset>
                    <legend>Decedent's Name (Include AKA's if any)</legend>
                        
                        <uL>
                            <li>
                                <label for="lastname">Last Name</label>
                                <?php
                                if (array_key_exists('lastname', $_POST) && !empty($_POST['lastname'])){
                                    echo $_POST['lastname'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                            
                            <li>
                                <label for="middlename">Middle Name</label>
                                 <?php
                                if (array_key_exists('middlename', $_POST) && !empty($_POST['middlename'])){
                                    echo $_POST['middlename'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                            
                            <li>
                                <label for="firstname">First Name</label>
                                <?php
                                if (array_key_exists('firstname', $_POST) && !empty($_POST['firstname'])){
                                    echo $_POST['firstname'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                            
                            <li>
                                <label for="date_of_birth">Date of Birth</label>
                                <?php
                                if (array_key_exists('date_of_birth', $_POST) && !empty($_POST['date_of_birth'])){
                                    echo $_POST['date_of_birth'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                            
                            <li>
                               
                                <label for="gender">Gender</label>
                                
                                <?php
                                if (array_key_exists('gender', $_POST) && !empty($_POST['gender'])){
                                    echo $_POST['gender'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                                                       
                            <li>
                                <label for="socal_security_number">Socal Security Number</label>
                                <?php
                                if (array_key_exists('socal_security_number', $_POST) && !empty($_POST['socal_security_number'])){
                                    echo $_POST['socal_security_number'];
                                }
                                else {
                                    echo "Not provided";
                                }  
                                ?>
                                
                            </li>
                            
                            <li>
                                <label for="facility_name">Facility Name</label>
                                <?php
                                if (array_key_exists('facility_name', $_POST) && !empty($_POST['facility_name'])){
                                    echo $_POST['facility_name'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                        </ul>
                    </fieldset><!--End Decedent's Name Field-->
                
                    <!--Start Decedent of Hispania Origin Field-->
                    <fieldset>
                        <legend>Decedent of Hispanic Origin</legend>
                            <h4>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decedent is not Spanish / Hispanic / Latino:</h4>
                              <?php
                                if (array_key_exists('origin', $_POST) && !empty($_POST['origin'])){
                                    echo $_POST['origin'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                    </fieldset><!--End Decedent of Hispania Origin Field-->
                
                    <!--Start Decedent's Race Field-->
                    <fieldset>
                        <legend>Decedent's Race</legend>
                        <h4>Check one or more races to indicate what the Decedent considered himself or be: </h4>
                              <?php
                                if (array_key_exists('race', $_POST) && !empty($_POST['race'])){
                                    echo $_POST['race'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                    </fieldset><!--End Decedent's Race Field-->
                    
                    <!--Start Certifies Death Field-->
                    <fieldset>
                        <legend>Items Must be Completed by Who Person Pronounces or Certifies Death.</legend><hr>
                        
                        <ul>
                           
                            <li>
                                <label for="date_pronounced_dead">Date Pronounced Dead</label>
                                <?php
                                if (array_key_exists('date_pronounced_dead', $_POST) && !empty($_POST['date_pronounced_dead'])){
                                    echo $_POST['date_pronounced_dead'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li> 
                            
                            <li>
                                <label for="time_pronounced_dead">Time Pronounced Dead</label>
                                <?php
                                if (array_key_exists('time_pronounced_dead', $_POST) && !empty($_POST['time_pronounced_dead'])){
                                    echo $_POST['time_pronounced_dead'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="signature_of_person">Signature Of Person Pronouncing Death</label>
                                <?php
                                if (array_key_exists('signature_of_person', $_POST) && !empty($_POST['signature_of_person'])){
                                    echo $_POST['signature_of_person'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                        
                            </li>
                                 
                            <li>
                                <label for="license_number">License Number</label>
                                <?php
                                if (array_key_exists('license_number', $_POST) && !empty($_POST['license_number'])){
                                    echo $_POST['license_number'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="date_signed">Date Signed</label>
                                <?php
                                if (array_key_exists('date_signed', $_POST) && !empty($_POST['date_signed'])){
                                    echo $_POST['date_signed'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="actual_date_of_birth">Actual Or Presumed Date Of Birth</label>
                                <?php
                                if (array_key_exists('actual_date_of_birth', $_POST) && !empty($_POST['actual_date_of_birth'])){
                                    echo $_POST['actual_date_of_birth'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="actual_time_of_death">Actual Or Presumed Time Of Death</label>
                                <?php
                                if (array_key_exists('actual_time_of_death', $_POST) && !empty($_POST['actual_time_of_death'])){
                                    echo $_POST['actual_time_of_death'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                                
                            </li>
                            
                            <li>
                                
                                <level>Was Medical Examiner Or Coroner Contacted</level><br> 
                                <?php
                                if (array_key_exists('language', $_POST) && !empty($_POST['language'])){
                                    echo $_POST['language'];
                                }
                                else {
                                    echo "Was Medical Examiner Or Coroner Contacted : Not provided";
                                }
                                ?>                           
                                
                            </li>
                        </ul>
                    </fieldset><!--End Certifies Death Field-->
                    
                    <!--Start Cause Of Death Field-->
                    <fieldset>
                        <legend>Cause Of Death ( See instructions and examples)</legend><hr>
                        <p>PART 1.Enter he chain of event - diseases, injuries, or complication --that directly caused the death............................. </p>
                        
                        <ul>
                            <li>
                                <label for="immediate_cause">a. Immediate Cause (Final disease or condition resulting in death)</label>
                                <?php
                                if (array_key_exists('immediate_cause', $_POST) && !empty($_POST['immediate_cause'])){
                                    echo $_POST['immediate_cause'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="due_to">Due to (or as a consequence of):</label>
                                <?php
                                if (array_key_exists('due_to_1', $_POST) && !empty($_POST['due_to_1'])){
                                    echo $_POST['due_to_1'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                                
                            </li>
                            
                            <li>
                                <label for="oneset_to_death">Onset to death</label>
                                <?php
                                if (array_key_exists('oneset_to_death_1', $_POST) && !empty($_POST['oneset_to_death_1'])){
                                    echo $_POST['oneset_to_death_1'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="list_conditions">b. Sequentially List Conditions,(if any, leading to the cause listed on line a.)</label>
                                <?php
                                if (array_key_exists('list_conditions', $_POST) && !empty($_POST['list_conditions'])){
                                    echo $_POST['list_conditions'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            </li>
                            
                            <li>
                                <label for="due_to">Due to (or as a consequence of):</label>
                                <?php
                                if (array_key_exists('due_to_2', $_POST) && !empty($_POST['due_to_2'])){
                                    echo $_POST['due_to_2'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="oneset_to_death">Onset to death</label>
                                <?php
                                if (array_key_exists('oneset_to_death_2', $_POST) && !empty($_POST['oneset_to_death_2'])){
                                    echo $_POST['oneset_to_death_2'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="underlying_cause">c. Enter the Underlying Cause, (disease or injury that initiated the events resulting in death)</label>
                                <?php
                                if (array_key_exists('underlying_cause', $_POST) && !empty($_POST['underlying_cause'])){
                                    echo $_POST['underlying_cause'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                                
                            </li>
                            
                            <li>
                                <label for="due_to">Due to (or as a consequence of):</label>
                                <?php
                                if (array_key_exists('due_to_3', $_POST) && !empty($_POST['due_to_3'])){
                                    echo $_POST['due_to_3'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                                
                            </li>
                            
                            <li>
                                <label for="oneset_to_death">Onset to death</label>
                                <?php
                                if (array_key_exists('oneset_to_death_3', $_POST) && !empty($_POST['oneset_to_death_3'])){
                                    echo $_POST['oneset_to_death_3'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="last">Last</label>
                                <?php
                                if (array_key_exists('last', $_POST) && !empty($_POST['last'])){
                                    echo $_POST['last'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="oneset_to_death">Onset to death</label>
                                <?php
                                if (array_key_exists('oneset_to_death_4', $_POST) && !empty($_POST['oneset_to_death_4'])){
                                    echo $_POST['oneset_to_death_4'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>    
                            </li>         
                        </ul>
                        
                        <p>PART 2. Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1  </p>
                        <ul>
                           <li>
                                <label for="part2"></label>
                                <?php
                                if (array_key_exists('part2', $_POST) && !empty($_POST['part2'])){
                                    echo $_POST['part2'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                             
                            </li> 
                        </ul>
                        
                        <ul>
                            <li>
                                <label for="autospy1">Was An Autospy Performed?</label><br>
                                <?php
                                if (array_key_exists('autospy1', $_POST) && !empty($_POST['autospy1'])){
                                    echo $_POST['autospy1'];
                                }
                                else {
                                    echo "Was An Autospy Performed? : Not provided";
                                }
                                ?>
                             
                            </li>
                            
                            <li>
                                <label for="autospy2">Were Autospy Findings Avaliable TO Complete The Cause of Death?</label><br>
                                <?php
                                if (array_key_exists('autospy2', $_POST) && !empty($_POST['autospy2'])){
                                    echo $_POST['autospy2'];
                                }
                                else {
                                    echo "Were Autospy Findings Avaliable TO Complete The Cause of Death? : Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="autospy3">Did Tobacco Use Contribute To Death?</label><br>
                                <?php
                                if (array_key_exists('autospy3', $_POST) && !empty($_POST['autospy3'])){
                                    echo $_POST['autospy3'];
                                }
                                else {
                                    echo "Did Tobacco Use Contribute To Death? : Not provided";
                                }
                                ?>
                                
                            </li>
                        </ul>
                    </fieldset>
                    <!--End Cause Of Death Field-->
                    
                    <!--Start If Female Field-->
                    <fieldset>
                        <legend>If Female:</legend><hr>
                        <?php
                                if (array_key_exists('female', $_POST) && !empty($_POST['female'])){
                                    echo $_POST['female'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                        
                    </fieldset>
                    <!--End If Female Field-->
                    
                    <!--Start Manner of Death Field-->
                    <fieldset>
                        <legend>Manner Of Death</legend><hr>
                        
                        <?php
                                if (array_key_exists('manner', $_POST) && !empty($_POST['manner'])){
                                    echo $_POST['manner'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                        
                    </fieldset>
                    <!--End Manner of Death Field-->
                    
                    <!--Start Injury Field-->
                    <fieldset>
                        <legend>Injury</legend><hr>
                        <ul>
                            <li>
                                <label for="date_of_injury">Date Of Injury</label>
                                <?php
                                if (array_key_exists('injury_date', $_POST) && !empty($_POST['injury_date'])){
                                    echo $_POST['injury_date'];
                                }
                                else {
                                    echo "Not provided"."<br/>";
                                }
                                ?>
                                
                                
                                <label for="time_of_injury">Time Of Injury</label>
                                <?php
                                if (array_key_exists('njury_time', $_POST) && !empty($_POST['njury_time'])){
                                    echo $_POST['njury_time'];
                                }
                                else {
                                    echo "Not provided"."<br/>";
                                }
                                ?>
                                
                                
                                <label for="place_of_injury">Place Of Injury</label>
                                <?php
                                if (array_key_exists('njury_place', $_POST) && !empty($_POST['njury_place'])){
                                    echo $_POST['njury_place'];
                                }
                                else {
                                    echo "Not provided"."<br/>";
                                }
                                ?>
                                
                                
                                <label for="injury_work">Injury At Work?</label>
                                <?php
                                if (array_key_exists('injury_work', $_POST) && !empty($_POST['injury_work'])){
                                    echo $_POST['injury_work'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                                
                              
                            </li>
                        </ul>
                    </fieldset>
                    <!--End Injury Field-->
                    
                    <!--Start Location of Injury Field-->
                    <fieldset>
                        <legend>Location Of Injury</legend><hr>
                        <ul>
                            <li>
                                <label for="state">State</label>
                                <?php
                                if (array_key_exists('state', $_POST) && !empty($_POST['state'])){
                                    echo $_POST['state'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="city_town">City Or Town</label>
                                <?php
                                if (array_key_exists('city_town', $_POST) && !empty($_POST['city_town'])){
                                    echo $_POST['city_town'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                            
                            </li>
                            
                            <li>
                                <label for="street_number">Street and Number</label>
                                <?php
                                if (array_key_exists('street_number', $_POST) && !empty($_POST['street_number'])){
                                    echo $_POST['street_number'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="apartment_no">Apartment No:</label>
                                <?php
                                if (array_key_exists('apartment_no', $_POST) && !empty($_POST['apartment_no'])){
                                    echo $_POST['apartment_no'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                            
                            <li>
                                <label for="zip_code">Zip Code</label>
                                <?php
                                if (array_key_exists('zip_code', $_POST) && !empty($_POST['zip_code'])){
                                    echo $_POST['zip_code'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                               
                            </li>
                        </ul>
                    </fieldset>
                    <!--End Location of Injury Field-->
                    
                    <!--Start Describe How Injury Occurred: Field-->
                    <fieldset>
                        <legend>Describe How Injury Occurred:</legend>
             
                            <label for="zip_code"></label>
                            <?php
                                if (array_key_exists('describe_injury', $_POST) && !empty($_POST['describe_injury'])){
                                    echo $_POST['describe_injury'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                      
                    </fieldset>
                    <!--End Describe How Injury Occurred: Field-->
                    
                    <!--Start Transportation Injury Field-->
                    <fieldset>
                        <legend>If Transportation Injury</legend><hr>
                        <h4>Specify:</h4>
                        <?php
                                if (array_key_exists('transportation', $_POST) && !empty($_POST['transportation'])){
                                    echo $_POST['transportation'];
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                        
                    </fieldset>
                    <!--End Transportation Injury Field-->
                    
                    <!--Start Certifier Field-->
                    <fieldset>
                        <legend>Certifier</legend>
                        <h4>Check Only One:</h4>
                        
                        <?php
                                if (array_key_exists('certifier', $_POST) && !empty($_POST['certifier'])){
                                    echo $_POST['certifier']."<br/>"."<br/>";
                                }
                                else {
                                    echo "Not provided"."<br/>";
                                }
                                ?>
                        
 
                                <label for="signature_certifier">Signature of Certifier</label><br>
                                <?php
                                if (array_key_exists('signature_certifier', $_POST) && !empty($_POST['signature_certifier'])){
                                    echo $_POST['signature_certifier']."<br/>";
                                }
                                else {
                                    echo "Not provided";
                                }
                                ?>
                  
                    </fieldset>
                    <!--End Certifier Field-->
                    
                    <!--Start Person Completing Cause Of Death Field-->
                    <fieldset>
                        <legend>Person Completing Cause Of Death</legend><hr>
                        <ul>
                            <li>
                                
                                <?php
                                if (array_key_exists('person_name', $_POST) && !empty($_POST['person_name'])){
                                    echo "Name : ".$_POST['person_name']."<br/>";
                                }
                                else {
                                    echo "Name : Not provided"."<br/>";
                                }
                                ?>
            
                                <?php
                                if (array_key_exists('person_address', $_POST) && !empty($_POST['person_address'])){
                                    echo "Address : ".$_POST['person_address']."<br/>";
                                }
                                else {
                                    echo "Address : Not provided"."<br/>";
                                }
                                ?>
                     
                       
                                <?php
                                if (array_key_exists('person_zip_code', $_POST) && !empty($_POST['person_zip_code'])){
                                    echo "ZIP Code : ".$_POST['person_zip_code']."<br/>";
                                }
                                else {
                                    echo "ZIP Code : Not provided"."<br/>";
                                }
                                ?>
                              
                            </li>
                        </ul> <hr>  
                        
                        <ul>
                            <li>
                               
                                <?php
                                if (array_key_exists('person_title', $_POST) && !empty($_POST['person_title'])){
                                    echo "Title Of Certifier : ".$_POST['person_title']."<br/>";
                                }
                                else {
                                    echo "Title Of Certifier : Not provided"."<br/>";
                                }
                                ?>
                         
                                <?php
                                if (array_key_exists('person_license_number', $_POST) && !empty($_POST['person_license_number'])){
                                    echo "License Number : ".$_POST['person_license_number']."<br/>";
                                }
                                else {
                                    echo "License Number : Not provided"."<br/>";
                                }
                                ?>
                            
                                <?php
                                if (array_key_exists('person_date', $_POST) && !empty($_POST['person_date'])){
                                    echo "Date Certified : ".$_POST['person_date']."<br/>";
                                }
                                else {
                                    echo "Date Certified : Not provided"."<br/>";
                                }
                                ?>
                          
                            </li>
                        </ul><hr>
                    </fieldset>
                    <!--End Person Completing Cause Of Death Field-->
                    
                <input type="submit" value="Save"/>     
            </form> 
                     
        </section>        
    </body>
</html>

