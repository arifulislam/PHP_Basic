<?php
session_start();

$forminfo = '';
if(array_key_exists('forminfo',$_SESSION)){
$forminfo = $_SESSION['allinfo'];
}
?>


<html>
    <head>
        <title></title>
    </head>
    
    <body>
        <section>
            <div>
                <p> <a href="newcreate.html"> Click Here </a> to fill with Decedent's personal info.</p>
            </div>
           
            <div align="center">
            <h1>Appendix A Sample US Death Certificate Form</h1>
                <p>The sample death reportin form............</p>
                <h2>Death Reporting Form Vital Records</h2>
            </div>
            <fieldset>
                        <legend><h2>Decedent's Name (Include AKA's if any)</h2></legend>
                        
            <table border="1" callpadding="30" align="center">
                <tr>
                    <th> Serial No. </th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                    <th>Social Security Number</th>
                    <th>Facility Name</th>
                    <th>Action</th>
                </tr>
                
                <?php
                    $counter=1;
                    foreach($forminfo as $value){
                ?>
                  
                <tr>
                    <td>
                       <?php echo $counter++ ?> 
                    </td>
                    
                    <td><?php echo $value['lastname'];?></td>
                    
                    <td><?php echo $value['firstname'];?></td>    
                  
                    <td><?php echo $value['middlename'];?></td> 
                    
                    <td><?php echo $value['date_of_dirth'];?></td>
                    
                    <td><?php echo $value['gender'];?></td>
                    
                    <td><?php echo $value['socal_security_number'];?></td>
                    
                    <td><?php echo $value['facility'];?></td>
                    
                    <td>
                        <a href="show.pho">View</a>
                        <a href="edit.pho">Edit</a>
                        <a href="delete.pho">Delete</a>
                    </td>
                    
                </tr>
       
                <?php } ?>
        
            </table>
        </fieldset>
            
            
            <fieldset>
                <legend><h2>Decedent of Hispanic Origin</h2></legend>
                <h4>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decedent is not Spanish / Hispanic / Latino:</h4>
                        
                <table border="1" callpadding="30" align="center">
               
                <tr>
                    <th> Serial No. </th>
                    <th>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decedent is not Spanish / Hispanic / Latino:</th>
                    <th>Action</th>
                </tr>
                
                <?php
                    $counter=1;
                    foreach($forminfo as $origin){
                ?>
                  
                <tr>
                    <td>
                       <?php echo $counter++ ?> 
                    </td>
                    
                    <td><?php echo $origin['origin'];?></td>
                    
                    <td>
                        <a href="show.pho">View</a>
                        <a href="edit.pho">Edit</a>
                        <a href="delete.pho">Delete</a>
                    </td>
                </tr>
       
                <?php } ?>
        
            </table>
        </fieldset>
        
            <!--Start Decedent's Race Field-->
                    <fieldset>
                        <legend><h2>Decedent's Race</h2></legend><hr>
                        <h3>Check one or more races to indicate what the Decedent considered himself or be: </h3>
                        <table border="1" callpadding="30" align="center">
                            
                        <tr>
                            <th> Serial No. </th>
                            <th>Check one or more races to indicate what the Decedent considered himself or be:</th>
                            <th>Action</th>
                        </tr>
                        
                        <?php
                            $counter=1;
                            foreach($forminfo as $race){
                            ?>   
                                
                        <tr>
                            <td>
                                <?php echo $counter++ ?> 
                            </td>
                            
                            <td> <?php echo $race['race'];?> </td>
                            
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>   
                                
                        <?php    }   ?>
                        </table>
                    </fieldset><!--End Decedent's Race Field-->
                    
                    <!--Start Certifies Death Field-->
                    <fieldset>
                        <legend><h2>Items Must be Completed by Who Person Pronounces or Certifies Death.</h2></legend><hr>
                        <table border="1" callpadding="30" align="center">
                            
                        <tr>
                            <th> Serial No. </th>
                            <th>Date Pronounced Dead</th>
                            <th>Time Pronounced Dead</th>
                            <th>Signature Of Person Pronouncing Death</th>
                            <th>License Number</th>
                            <th>Date Signed</th>
                            <th>Actual Or Presumed Date Of Birth</th>
                            <th>Actual Or Presumed Time Of Death</th>
                            <th>Was Medical Examiner Or Coroner Contacted</th>
                            <th>Action</th>
                        </tr>
                        
                        <?php
                            $counter=1;
                            foreach($forminfo as $death){
                            ?>    
                            
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $death['date_pronounced_dead'];?></td>
                            <td><?php echo $death['time_pronounced_dead'];?></td>
                            <td><?php echo $death['signature_of_person'];?></td>
                            <td><?php echo $death['license_number'];?></td>
                            <td><?php echo $death['date_signed'];?></td>
                            <td><?php echo $death['actual_date_of_birth'];?></td>
                            <td><?php echo $death['actual_time_of_death'];?></td>
                            <td><?php echo $death['language'];?></td>
                           
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                                
                        <?php   }?>
                        
                        </table>
                    </fieldset><!--End Certifies Death Field-->
                    
           <!--Start Cause Of Death Field-->
                    <fieldset>
                        <legend><h2>Cause Of Death ( See instructions and examples)</h2></legend><hr>
                        <p>PART 1.Enter he chain of event - diseases, injuries, or complication --that directly caused the death............................. </p>
                        <table border="1" callpadding="30" align="center">
                            
                          <tr>
                            <th> Serial No. </th>
                            <th>a. Immediate Cause</th>
                            <th>Due to</th>
                            <th>Onset to death</th>
                            <th>b. Sequentially List Conditions</th>
                            <th>Due to</th>
                            <th>Onset to death</th>
                            <th>c. Enter the Underlying Cause</th>
                            <th>Due to</th>
                            <th>Last</th>
                            <th>Onset to death</th>
                            <th>Action</th>
                        </tr>  
                          
                        <?php
                        $counter=1;
                        foreach ($forminfo as $cause){
                        ?>    
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $cause['immediate_cause']; ?></td>
                            <td><?php echo $cause['due_to_1']; ?></td>
                            <td><?php echo $cause['oneset_to_death_1']; ?></td>
                            <td><?php echo $cause['list_conditions']; ?></td>
                            <td><?php echo $cause['due_to_2']; ?></td>
                            <td><?php echo $cause['oneset_to_death_2']; ?></td>
                            <td><?php echo $cause['underlying_cause']; ?></td>
                            <td><?php echo $cause['due_to_3']; ?></td>
                            <td><?php echo $cause['oneset_to_death_3']; ?></td>
                            <td><?php echo $cause['last']; ?></td>
                            <td><?php echo $cause['oneset_to_death_4']; ?></td>
                            
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                        
                        <?php  }?>
                        </table>
                        
                        <p><h3>PART 2. Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1</h3></p>
                        <table border="1" callpadding="30" align="center">
                            
                        <tr>
                            <th> Serial No. </th>
                            <th>PART 2. Enter other significant conditions contributing to death but not resulting in the underlying cause given in PART 1</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $counter=1;
                        foreach ($forminfo as $part2){
                        ?>    
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $part2['part2']; ?></td>
                           
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                        <?php }?>
                        </table><br>
                        
                       
                        <table border="1" callpadding="30" align="center">  
                        <tr>
                            <th> Serial No. </th>
                            <th>Was An Autospy Performed?</th>
                            <th>Were Autospy Findings Avaliable TO Complete The Cause of Death?</th>
                            <th>Did Tobacco Use Contribute To Death?</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $counter=1;
                        foreach ($forminfo as $autospy ){
                        ?>    
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $autospy ['autospy1']; ?></td>
                            <td><?php echo $autospy ['autospy2']; ?></td>
                            <td><?php echo $autospy ['autospy3']; ?></td>
                           
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                        <?php }?>
                        </table><br>
                    </fieldset>
                    <!--End Cause Of Death Field-->
                    
                    <!--Start If Female Field-->
                    <fieldset>
                        <legend><h2>If Female:</h2></legend><hr>
                        <table border="1" callpadding="30" align="center">  
                        <tr>
                            <th> Serial No. </th>
                            <th>If Female:</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $counter=1;
                        foreach ($forminfo as $female ){
                        ?>    
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $female ['female']; ?></td>
                         
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                        <?php }?>
                        </table><br>
                    </fieldset>
                    <!--End If Female Field-->
                    
                    <!--Start Manner of Death Field-->
                    <fieldset>
                        <legend><h2>Manner Of Death</h2></legend><hr>
                        <table border="1" callpadding="30" align="center">  
                        <tr>
                            <th> Serial No. </th>
                            <th>Manner Of Death</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $counter=1;
                        foreach ($forminfo as $manner ){
                        ?>    
                        <tr>
                            <td><?php echo $counter++ ?></td>
                            
                            <td><?php echo $manner ['manner']; ?></td>
                         
                            <td>
                                <a href="show.pho">View</a>
                                <a href="edit.pho">Edit</a>
                                <a href="delete.pho">Delete</a>
                            </td>
                        </tr>
                        <?php }?>
                        </table><br>
                    </fieldset>
                    <!--End Manner of Death Field-->         
      </section>  
    </body>
</html>
