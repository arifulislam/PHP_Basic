<?php

$var = ' ';

if (isset($var)) {
    echo "This var is set so I will print.";
    
}

$a = "test";
$b = "amothertest";

var_dump(isset($a));
var_dump(isset($a, $b));

unset ($a);


var_dump(isset($a));
var_dump(isset($a, $d));

$foo = NULL;
var_dump(isset($a, $b));