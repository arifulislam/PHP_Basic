<?php
session_start ();

$alldata = $_SESSION['formdata'];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo constant('PAGE_TITLE'); ?> </title>
    </head>
    <body>
        <section>
            <div>
                <p> <a href="finalcreate.html"> Click Here </a> to fill with Decedent's personal info.</p>
            </div>
           
            <table border="1">
                <tr>
                    <th> Serial No. </th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                    <th>Social Security Number</th>
                    <th>Facility Name</th>
                    <th colspan="3">Manage Individual Info</th>
                
                </tr>
                
                <?php
                    $counter=1;
                    foreach($alldata as $value){
                ?>
                <tr>
                    <td>
                        <?php echo $counter++ ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('lname',$value) && !empty($value['lname']))
                            {
                                echo $value['lname'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                        <td>
                        <?php 
                        
                            if (array_key_exists('fname',$value) && !empty($value['fname']))
                            {
                                echo $value['fname'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('mname',$value) && !empty($value['mname']))
                            {
                                echo $value['mname'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('date_of_brith',$value) && !empty($value['date_of_brith']))
                            {
                                echo $value['date_of_brith'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('gender',$value) && !empty($value['gender']))
                            {
                                echo $value['gender'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('socal_security_numaber',$value) && !empty($value['socal_security_numaber']))
                            {
                                echo $value['socal_security_numaber'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    
                    <td>
                        <?php 
                        
                            if (array_key_exists('facility',$value) && !empty($value['facility']))
                            {
                                echo $value['facility'];
                            }
                            else {
                                echo "Not Provided";
                            }
                            ?>
                    </td>
                    <td>
                    <a href="show.php">View</a>
                    <a href="edit.php">Edit</a>
                    <a href="delete.php">Delete</a>
                    </td>>
                </tr>
                <?php }?>
                
            </table>
        </section>
    </body>
</html>